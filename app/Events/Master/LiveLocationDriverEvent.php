<?php

namespace App\Events\Master;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LiveLocationDriverEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $postdata;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($postdata)
    {
        $this->postdata = $postdata;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trentsend-app');
    }

    public function broadcastAs()
    {
        return 'liveLocationDriverEvent';
    }
}
