<?php

namespace App\Events\Trans;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TransOrderReceivedDriverEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $postdata;
    public $driverCode;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($postdata, $driverCode)
    {
        $this->postdata = $postdata;
        $this->driverCode = $driverCode;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trentsend-app');
    }

    public function broadcastAs()
    {
        return 'orderReceivedDriverEvent-'.$this->driverCode;
    }
}
