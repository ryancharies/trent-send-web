<?php

namespace App\Events\Trans;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TransOrderDriverEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $postdata;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($postdata)
    {
        $this->postdata = $postdata;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('trentsend-app');
    }

    public function broadcastAs()
    {
        return 'orderReceivedDriverEvent';
    }
}
