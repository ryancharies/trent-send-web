<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverComment extends Model
{
    use HasFactory;
    protected $table = 'dt_driver_comment';
    protected $guarded = [];
}
