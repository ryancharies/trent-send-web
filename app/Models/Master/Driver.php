<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;

class Driver extends Model
{
    use HasFactory, SoftDeletes, HasApiTokens;
    protected $table = 'mst_driver';
    protected $guarded = [];

    /**
     * @return mixed
     */
    public function avgStar()
    {
        return $this->hasMany(DriverComment::class, 'driver_id','id')->average('star');
    }
}
