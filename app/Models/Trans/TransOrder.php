<?php

namespace App\Models\Trans;

use App\Models\Data\PaymentMethod;
use App\Models\Data\Service;
use App\Models\Master\Customer;
use App\Models\Master\Driver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransOrder extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'trans_orders';
    protected $guarded = [];
    protected $dates = ['order_date','done_at','schedule_at','canceled_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function paymentMethod()
    {
        return $this->hasOne(PaymentMethod::class,'id','payment_method_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function service()
    {
        return $this->hasOne(Service::class, 'id','service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function driver()
    {
        return $this->hasOne(Driver::class, 'id','driver_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id','customer_id');
    }


}
