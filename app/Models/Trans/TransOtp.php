<?php

namespace App\Models\Trans;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UUID;

class TransOtp extends Model
{
    use HasFactory, SoftDeletes, UUID;
    protected $table = 'trans_otp';
    protected $guarded = [];
    public $incrementing = false;
    protected $keyType = 'uuid';

}
