<?php

function transStatus($status){
    $arr = [0 => 'Non Aktif', 1 => 'Aktif'];
    return $arr[$status];
}

function transOnline($status){
    $arr = [0 => 'Offline', 1 => 'Online'];
    return $arr[$status];
}

function transPartnerType($partnerType){
    $arr = ['internal' => 'Internal', 'eksternal' => 'Eksternal'];
    return $arr[$partnerType];
}

function transStatusOrder($status){
    $arr = ['all' => 'Semua Transaksi','done' => 'Selesai','cancel' => 'Dibatalkan', 'expired' => 'Kadaluarsa'];
    return $arr[$status];
}

function transDeliveryStatus($is_done, $is_canceled){
    return $is_done ? 'Selesai' : ($is_canceled ? 'Dibatalkan' : 'Kadaluarsa');
}

function isDone($status){
    return $status ? 'Selesai' : 'Dalam Proses';
}

function isCanceled($status){
    return $status ? 'Dibatalakan' : '-';
}

function createInvoiceCode($type = 'instant'){
    $charCode = $type == 'instant' ? 'INS' : 'ETC';
    return 'INV-'.$charCode.date('dmy').rand(1000,9999);
}

function createDriverCode($type, $initial){
    $charCode = $type == 'internal' ? 0 : 1;
    return 'DRV-'.$initial.$charCode.rand(1000,9999);
}

function createCustomerCode($initial){
    $initial = createInitial($initial);
    return 'CST-'.$initial.rand(1000,9999);
}

function createInitial($string){
    $arr = explode(' ', $string);
    $initial = '';
    foreach($arr as $word)
    {
        $initial .= substr($word, 0, 1);
    }
    return $initial;
}

function formatRupiah($number, $withPrefix = true){
    $prefix = 'Rp ';
    $formatted = number_format($number,0,',','.');
    return $withPrefix ? $prefix.$formatted : $formatted;
}

function formatDate($date){
    return \Carbon\Carbon::parse($date);
}

function printDateTime($format = null){
    return (!is_null($format)) ? \Carbon\Carbon::now()->format($format) : \Carbon\Carbon::now();
}

function imagePath($add_path = ''){
    return asset('/storage/app/images/').$add_path;
}

function partnerType($type = 'enum'){
    $arr = ['internal' => 'Internal', 'eksternal' => 'Eksternal'];
    if($type == 'enum'){
        return str_replace('=', '|', http_build_query($arr, null, ';'));
    }
    return $arr;
}

function uomRate($type = 'enum'){
    $arr = ['km' => 'Kilometer'];
    if($type == 'enum'){
        return str_replace('=', '|', http_build_query($arr, null, ';'));
    }
    return $arr;
}

function imagePathInstant($item_image){
    return asset('trans_order/instant/'.$item_image);
}
