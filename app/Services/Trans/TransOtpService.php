<?php

namespace App\Services\Trans;

use App\Models\Trans\TransOtp;

class TransOtpService
{

    public function store($postdata = [])
    {
        return TransOtp::create($postdata);
    }

    public function find($postdata = [])
    {
        return TransOtp::where($postdata)->latest()->first();
    }

    public function remove($uuid)
    {
        return TransOtp::where('id',$uuid)->delete();
    }

}
