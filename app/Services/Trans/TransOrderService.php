<?php

namespace App\Services\Trans;

use App\Events\Trans\TransOrderCreatedEvent;
use App\Http\Resources\Trans\DetailInstantCustomerResource;
use App\Http\Resources\Trans\DetailInstantDriverResource;
use App\Http\Resources\Trans\ListInstantCustomerResource;
use App\Http\Resources\Trans\ListInstantDriverResource;
use App\Models\Data\PaymentMethod;
use App\Models\Data\Service;
use App\Models\Master\Driver;
use App\Models\Master\DriverComment;
use App\Models\Trans\TransOrder;
use App\Services\Master\CustomerService;
use App\Services\Master\DriverService;
use Carbon\Carbon;

/**
 * Class TransOrderService
 * @package App\Services
 */
class TransOrderService
{
    /**
     * @param $user_role
     * @param $postdata
     * @return mixed
     */
    public function listInstant($user_role, $postdata = [])
    {
        return ($user_role == 'customer') ? self::listInstantCustomer($postdata) : self::listInstantDriver($postdata);
    }

    /**
     * @param $user_role
     * @param $postdata
     * @return array
     */
    public function detailInstant($user_role, $postdata = [])
    {
        return ($user_role == 'customer') ? self::detailInstantCustomer($postdata) : self::detailInstantDriver($postdata);
    }

    /**
     * @param $postdata
     * @return array
     */
    public function listInstantCustomer($postdata)
    {
        $user = (new CustomerService())->find(['phone_number' => $postdata['phone_number']]);
        $data = TransOrder::where('customer_id', $user->id)->where('is_done', $postdata['is_done'])->get();

        return [
            'data' => ListInstantCustomerResource::collection($data),
            'message' => 'List instant customer'
        ];
    }

    /**
     * @param $invoice
     * @return array
     */
    public function detailInstantCustomer($invoice)
    {
        $data = TransOrder::with('driver')->where('invoice', $invoice)->first();
        return [
            'data' => new DetailInstantCustomerResource($data),
            'message' => 'Detail '.$invoice.' instant customer'
        ];
    }

    /**
     * @param $postdata
     * @return array
     */
    public function listInstantDriver($postdata)
    {
        $user = (new DriverService())->find(['phone_number' => $postdata['phone_number']]);
        $data = TransOrder::with(['customer','paymentMethod'])->where('driver_id', $user->id)->orderByDesc('order_date')->get();

        return [
            'data' => ListInstantDriverResource::collection($data),
            'message' => 'List instant driver'
        ];
    }

    /**
     * @param $invoice
     * @return array
     */
    public function detailInstantDriver($invoice)
    {
        $data = TransOrder::with(['customer','paymentMethod'])->where('invoice', $invoice)->first();
        return [
            'data' => new DetailInstantDriverResource($data),
            'message' => 'Detail '.$invoice.' instant driver'
        ];
    }

    public function activeInstant($phone_number)
    {
        $driver = (new DriverService())->find(['phone_number' => $phone_number]);
        $query = [
            'is_done' => 0,
            'is_canceled' => 0,
            'is_proceed' => 1,
            'driver_id' => $driver->id
        ];

        $data = TransOrder::where($query)->firstOr(function () use ($query){
            $query['is_proceed'] = 0;
            return TransOrder::where($query)->first();
        });

        return [
            'data' => $data ? new DetailInstantDriverResource($data) : null,
            'message' => 'An order active!'
        ];
    }

    /**
     * @return mixed
     */
    public function getTransOrderScheduled()
    {
        return TransOrder::where([
            'is_done' => 0,
            'is_scheduled' => 1
        ])->whereDate('order_date','>=',date('Y-m-d'))->get();
    }

    /**
     * @return mixed
     */
    public function getTransOrderNew()
    {
        return TransOrder::where([
            'is_done' => 0,
            'is_canceled' => 0,
            'is_proceed' => 0,
            'is_scheduled' => 0
        ])->whereDate('order_date', date('Y-m-d'))->get();
    }

    /**
     * @param $postdata
     * @return array
     */
    public function storeInstant($postdata = [])
    {
        $postdata['order_date'] = formatDate($postdata['order_date']);

        $now = Carbon::createMidnightDate();
        $order_date = Carbon::createMidnightDate($postdata['order_date']->year, $postdata['order_date']->month, $postdata['order_date']->day);
        $diffInDays = $now->diffInDays($order_date->setTime(0,0,0));

        $postdata['invoice'] = createInvoiceCode();
        $postdata['payment_method_id'] = PaymentMethod::where('code', $postdata['payment_method'])->value('id');
        $postdata['service_id'] = Service::where('code', $postdata['service'])->value('id');
        $postdata['is_scheduled'] = $diffInDays > 0 ? 1 : 0;
        $postdata['schedule_at'] = $postdata['is_scheduled'] ? $postdata['order_date'] : null;
        $postdata['order_date'] = $postdata['is_scheduled'] ? $now : $postdata['order_date'];
        $postdata['customer_id'] = (new CustomerService())->find(['phone_number' => $postdata['customer']])->id;

        unset($postdata['customer'],$postdata['payment_method'],$postdata['service']);

        $newTransOrder = TransOrder::create($postdata);
        $newTransOrder->created_at = $newTransOrder->created_at->format('d-m-Y H:i');
        TransOrderCreatedEvent::dispatch($newTransOrder);

        return ['message' => 'Instant order created! '.$postdata['invoice'], 'data' => null];
    }

    /**
     * @param $invoice
     * @param $postdata
     * @return array
     */
    public function updateInstant($invoice, $postdata = [])
    {
        $order = TransOrder::where('invoice', $invoice)->update($postdata);
        return ['message' => 'Instant order updated!', 'data' => null];
    }

    /**
     * @param $invoice
     * @param $postdata
     * @return array
     */
    public function cancelInstant($invoice, $postdata = [])
    {
        $order = TransOrder::where('invoice', $invoice)->update($postdata);
        return ['message' => 'Instant order canceled!', 'data' => null];
    }

    public function rejectInstant($invoice, $postdata = [])
    {
        self::updateInstant($invoice, [
            'driver_id' => null,
            'is_proceed' => 0,
        ]);

    }

    /**
     * @param $invoice
     * @return array
     */
    public function deleteInstant($invoice)
    {
        $order = TransOrder::where('invoice', $invoice)->delete();
        return ['message' => 'Instant order deleted!', 'data' => null];
    }

    /**
     * @param $invoice
     * @return mixed
     */
    public function find($invoice)
    {
        return TransOrder::where('invoice', $invoice)->first();
    }

    /**
     * @param $invoice
     * @param $postdata
     * @return string[]
     */
    public function commentInstant($invoice, $postdata = [])
    {
        $order = self::find($invoice);
        if(is_null($order->driver_id)){
            return ['message' => 'This order doesnt have driver!', 'error' => true];
        }
        DriverComment::updateOrCreate(
            ['invoice' => $invoice],
            ['driver_id' => $order->driver_id, 'star' => $postdata['star'], 'comment' => $postdata['comment']]
        );
        return ['message' => 'Comment for driver added!'];
    }

    public function getTransOrderReport($postdata = [])
    {
        $start = formatDate($postdata['start']);
        $end = formatDate($postdata['end']);
        $data = TransOrder::with(['customer','driver'])
                            ->when($postdata['status'] == 'done', function ($query){
                                $query->where('is_done', 1);
                            })
                            ->when($postdata['status'] == 'cancel', function ($query){
                                $query->where('is_canceled', 1);
                            })
                            ->when($postdata['status'] == 'expired', function ($query){
                                $query->where(['is_done' => 0, 'is_canceled' => 0]);
                            })
                            ->whereBetween('order_date',[$start,$end])
                            ->get();
        return $data;
    }

    public function countMonthData()
    {
        return TransOrder::where('is_done', 1)->whereMonth('order_date', date('m'))->count();
    }

    public function countYearData()
    {
        return TransOrder::where('is_done', 1)->whereYear('order_date', date('Y'))->count();
    }
}
