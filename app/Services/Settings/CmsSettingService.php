<?php

namespace App\Services\Settings;

use App\Models\Settings\CmsSetting;

class CmsSettingService
{
    public function update($name, $content)
    {
        return CmsSetting::where('name', $name)->update(['content' => $content]);
    }

    public function content($name)
    {
        return CmsSetting::firstWhere('name', $name)->content;
    }

}
