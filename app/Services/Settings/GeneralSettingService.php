<?php

namespace App\Services\Settings;

use App\Models\Settings\GeneralSetting;

class GeneralSettingService
{

    public function upsert($postdata = [])
    {
        try {
            foreach ($postdata as $data) {
                GeneralSetting::updateOrCreate([
                    'slug' => $data['slug'],
                    'name' => $data['name']
                ], ['content' => $data['content']]);
            }
            return ['error' => false, 'message' => 'ok'];
        }catch (\Exception $e){
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function find($postdata = [])
    {
        if(count($postdata) == 1){
            return GeneralSetting::where('name', $postdata[0])->first();
        }
        return GeneralSetting::whereIn('name', $postdata)->get();
    }

}
