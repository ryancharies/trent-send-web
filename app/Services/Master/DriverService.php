<?php

namespace App\Services\Master;

use App\Events\Master\DriverOnlineEvent;
use App\Models\Master\Driver;

class DriverService
{
    /**
     * @param $postdata
     * @return bool
     */
    public function find($postdata = [])
    {
        return Driver::where($postdata)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Driver::find($id);
    }

    /**
     * @param array $query
     * @param array $postdata
     * @return string[]
     */
    public function update(array $query = [], array $postdata = [])
    {
        Driver::where($query)->update($postdata);
        return ['message' => 'Update successfully!'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return Driver::find($id)->delete();
    }

    public function getOnline()
    {
         return Driver::all();
    }

    public function setAvailable($phone_number, $is_available = 1)
    {
        self::update(['phone_number' => $phone_number],['is_available' => $is_available]);
        self::dispatchEvent($phone_number);
        return ['message' => 'Available driver is set!'];
    }

    public function setOnline($phone_number, $is_online = 1)
    {
        self::update(['phone_number' => $phone_number],['is_available' => $is_online]);
        self::dispatchEvent($phone_number);
        return ['message' => 'Online driver is set!'];
    }

    public function dispatchEvent($phone_number)
    {
        $driver = self::find(['phone_number' => $phone_number]);
        DriverOnlineEvent::dispatch($driver);
    }

    public function countData()
    {
        return Driver::count();
    }
}
