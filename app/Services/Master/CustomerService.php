<?php

namespace App\Services\Master;

use App\Models\Master\Customer;

class CustomerService
{
    public function all()
    {
        return Customer::all();
    }

    /**
     * @param $postdata
     * @return bool
     */
    public function find($postdata = [])
    {
        return Customer::where($postdata)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Customer::find($id);
    }

    /**
     * @param $postdata
     * @return mixed
     */
    public function store($postdata = [])
    {
        $user = Customer::create($postdata);
        return ['message' => 'User has created!', 'data' => $user];
    }

    /**
     * @param $query
     * @param $postdata
     * @return mixed
     */
    public function update($query = [], $postdata = [])
    {
        Customer::where($query)->update($postdata);
        return ['message' => 'Update successfully!'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return Customer::find($id)->delete();
    }

    public function countData()
    {
        return Customer::count();
    }
}
