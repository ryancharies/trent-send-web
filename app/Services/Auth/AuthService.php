<?php

namespace App\Services\Auth;

use App\Http\Resources\Auth\AuthResource;
use App\Services\Master\CustomerService;
use App\Services\Master\DriverService;
use App\Services\Notifications\NotificationService;
use App\Services\Trans\TransOtpService;

class AuthService
{
    /**
     * @param $postdata
     * @return array|string[]
     */
    public function login($user_role, $postdata = [])
    {
        if($user_role == 'customer'){
            $user = (new CustomerService())->find(['phone_number' => $postdata['phone_number']]);
        }else{
            $user = (new DriverService())->find(['phone_number' => $postdata['phone_number']]);
        }

        if(empty($user)){
            return ['message' => 'Phone number not registered', 'code' => 404];
        }

        $codeOtp = rand(1000,9999);
        $otp = (new TransOtpService())->store([
            'phone_number' => $postdata['phone_number'],
            'user_role' => $user_role,
            'otp' => $codeOtp
        ]);

        if(!$otp){
            return ['message' => 'Failed create code otp!', 'code' => 500];
        }

        $sendOtp = (new NotificationService())->sendMessageWhatsapp([
            'phone_number' => $user->phone_number,
            'otp' => $codeOtp
        ]);

        return ['message' => 'OTP has been created and sent | '.$codeOtp];
    }

    /**
     * @param $postdata
     * @return mixed
     */
    public function registerCustomer($postdata = [])
    {
        $postdata['code'] = createCustomerCode($postdata['name']);
        $result = (new CustomerService())->store($postdata);
        return ['message' => $result['message']];
    }

    /**
     * @param $postdata
     * @return array
     */
    public function verifyOtp($user_role, $postdata = [])
    {
        $otp = (new TransOtpService())->find([
            'phone_number' => $postdata['phone_number'],
            'user_role' => $user_role
        ]);

        if ($otp->otp != $postdata['otp']){
            return ['verified' => false, 'message' => 'OTP didnt match!', 'code' => 404];
        }

        if($user_role == 'driver'){
            (new DriverService())->update(['phone_number' => $postdata['phone_number']],['fcm_token' => $postdata['fcm_token']]);
        }else{
            (new CustomerService())->update(['phone_number' => $postdata['phone_number']],['fcm_token' => $postdata['fcm_token']]);
        }

        (new TransOtpService())->remove($otp->id);
        return ['verified' => true, 'message' => 'OTP verified!'];
    }

    /**
     * @param $user_role
     * @param $postdata
     * @return array|string[]
     */
    public function resendOtp($user_role, $postdata = [])
    {
        $transOtpService = new TransOtpService();
        $otp = $transOtpService->find([
            'phone_number' => $postdata['phone_number'],
            'user_role' => $user_role
        ]);
        $transOtpService->remove($otp->id);

        $codeOtp = rand(1000,9999);
        $otp = $transOtpService->store([
            'phone_number' => $postdata['phone_number'],
            'user_role' => $user_role,
            'otp' => $codeOtp
        ]);

        if(!$otp){
            return ['message' => 'Failed create code otp!', 'code' => 500];
        }

//        $sendOtp = (new NotificationService())->sendMessageWhatsapp([
//            'phone_number' => $postdata['phone_number'],
//            'otp' => $codeOtp
//        ]);

        return ['message' => 'OTP has been created and sent | '.$codeOtp];
    }

    /**
     * @param $postdata
     * @return AuthResource
     */
    public function createToken($user_role, $postdata = [])
    {
        if($user_role == 'customer'){
            $user = (new CustomerService())->find(['phone_number' => $postdata['phone_number']]);
            $user->role = 'Customer';
        }else{
            $user = (new DriverService())->find(['phone_number' => $postdata['phone_number']]);
            $user->role = 'Driver';
        }

        $user->type_token = 'Bearer';
        $user->token = $user->createToken(env('APP_TOKEN'))->plainTextToken;

        return new AuthResource($user);
    }

    /**
     * @param $user_role
     * @param $postdata
     * @return string[]
     */
    public function logout($user_role, $postdata = [])
    {
        if($user_role == 'customer'){
            $user = (new CustomerService())->find(['phone_number' => $postdata['phone_number']]);
        }else{
            $driverService = new DriverService();
            $user = $driverService->find(['phone_number' => $postdata['phone_number']]);
            $driverService->update(['phone_number' => $postdata['phone_number']],['is_online' => 0]);
        }

        $user->tokens()->where('id', $postdata['token'])->delete();
        return ['message' => 'Logged Out!'];
    }

}
