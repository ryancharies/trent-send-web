<?php

namespace App\Services\Notifications;

use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\Http;

class NotificationService
{
    public function sendMessageWhatsapp($postdata = [])
    {
        return Http::post('https://app.whacenter.com/api/send',[
            'device_id' => CRUDBooster::getSetting('device_id'),
            'number' => '62'.$postdata['phone_number'],
            'message' => 'JANGAN MEMBERITAHU KODE RAHASIA INI KE SIAPAPUN termasuk pihak Trent Send. WASPADA TERHADAP KASUS PENIPUAN! KODE VERIFIKASI untuk masuk: '.$postdata['otp']
        ]);
    }
}
