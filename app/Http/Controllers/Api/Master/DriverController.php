<?php

namespace App\Http\Controllers\Api\Master;

use App\Events\Master\DriverLocationEvent;
use App\Events\Master\DriverOnlineEvent;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Master\DriverRequest;
use App\Http\Resources\Master\DriverResource;
use App\Services\Master\DriverService;
use Illuminate\Http\Request;

class DriverController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param DriverService $driverService
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request, DriverService $driverService)
    {
        $postdata = $request->validate([
            'phone_number' => 'required'
        ]);

        try {
            $result = $driverService->find(['phone_number' => $postdata['phone_number']]);
            return $this->response(new DriverResource($result));
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param DriverRequest $request
     * @param DriverService $driverService
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DriverRequest $request, DriverService $driverService)
    {
        try {
            $filename = null;
            if($request->hasFile('photo')){
                $path = $request->photo->store('images/master/driver');
                $file = explode('/',$path);
                $filename = end($file);
            }
            $result = $driverService->update(
                ['phone_number' => $request->phone_number],
                [
                    'name' => $request->name,
                    'photo' => $filename
                ]
            );
            return $this->response(null, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function setStatus(Request $request, DriverService $driverService)
    {
        $postdata = $request->validate([
            'phone_number' => 'required|string',
            'is_online' => 'required|integer'
        ]);

        try {
            $result = $driverService->setOnline($postdata['phone_number'],$postdata['is_online']);
            return $this->response(null, $result['message'].' | Status '.transOnline($postdata['is_online']));
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
