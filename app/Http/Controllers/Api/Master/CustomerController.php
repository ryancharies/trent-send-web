<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Master\CustomerRequest;
use App\Http\Resources\Master\CustomerResource;
use App\Services\Master\CustomerService;
use Illuminate\Http\Request;

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param CustomerService $customerService
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request, CustomerService $customerService)
    {
        $postdata = $request->validate([
            'phone_number' => 'required'
        ]);

        try {
            $result = $customerService->find(['phone_number' => $postdata['phone_number']]);
            return $this->response(new CustomerResource($result));
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomerRequest $request, CustomerService $customerService)
    {
        try {
            $filename = null;
            if($request->hasFile('photo')){
                $path = $request->photo->store('images/master/customer');
                $file = explode('/',$path);
                $filename = end($file);
            }
            $result = $customerService->update(
                ['phone_number' => $request->phone_number],
                [
                    'name' => $request->name,
                    'photo' => $filename
                ]
            );
            return $this->response(null, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
