<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * @param $data
     * @param $message
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function response($data = null, $message = 'success', $code = 200)
    {
        return response()->json([
            'error' => false,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    /**
     * @param $message
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function error($message = '', $code = 500)
    {
        return response()->json([
            'error' => true,
            'message' => 'Oops! Something wrong happen.',
            'message_error' => $message
        ], $code);
    }
}
