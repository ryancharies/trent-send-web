<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\LogoutRequest;
use App\Http\Requests\Auth\RegisterCustomerRequest;
use App\Http\Requests\Auth\VerifyOtpRequest;
use App\Services\Auth\AuthService;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login($user_role, LoginRequest $request, AuthService $authService)
    {
        try {
            $result = $authService->login($user_role, $request->validated());
            if($result['code']){
                return $this->error($result['message'], $result['code']);
            }
            return $this->response(null, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param RegisterCustomerRequest $request
     * @param AuthService $authService
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerCustomer(RegisterCustomerRequest $request, AuthService $authService)
    {
        try {
            $result = $authService->registerCustomer($request->validated());
            if($result['code']){
                return $this->error($result['message'], $result['code']);
            }
            return $this->response(null, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param VerifyOtpRequest $request
     * @param AuthService $authService
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyOtp($user_role, VerifyOtpRequest $request, AuthService $authService)
    {
        try {
            $result = $authService->verifyOtp($user_role, $request->validated());
            if(!$result['verified']){
                return $this->error($result['message'], $result['code']);
            }
            $auth = $authService->createToken($user_role, $request->only(['phone_number']));
            return $this->response($auth, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $user_role
     * @param LoginRequest $request
     * @param AuthService $authService
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendOtp($user_role, LoginRequest $request, AuthService $authService)
    {
        try {
            $result = $authService->resendOtp($user_role, $request->validated());
            if($result['code']){
                return $this->error($result['message'], $result['code']);
            }
            return $this->response(null, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $user_role
     * @param LogoutRequest $request
     * @param AuthService $authService
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout($user_role, LogoutRequest $request, AuthService $authService)
    {
        $postdata = $request->validated();
        $postdata['token'] = $request->bearerToken();
        try {
            $result = $authService->logout($user_role, $postdata);
            return $this->response(null, $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }
}
