<?php

namespace App\Http\Controllers\Api\Trans;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Trans\TransOrderRequest;
use App\Services\Master\DriverService;
use App\Services\Trans\TransOrderService;
use Illuminate\Http\Request;

class TransOrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @param $user_role
     * @param Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function listInstant($user_role, Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'phone_number' => 'required',
            'is_done' => ''
        ]);

        try {
            $result = $transOrderService->listInstant($user_role, $postdata);
            return $this->response($result['data'], $result['message']);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function detailInstant($user_role, Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'invoice' => 'required'
        ]);

        try {
            $result = $transOrderService->detailInstant($user_role, $postdata['invoice']);
            return $this->response($result['data'], $result['message']);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeInstant(TransOrderRequest $request, TransOrderService $transOrderService)
    {
        try {
            $result = $transOrderService->storeInstant($request->validated());
            return $this->response($result['data'], $result['message']);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeImage(Request $request)
    {
        try {
            $path = $request->image->store('images/trans/order/instant');
            $file = explode('/',$path);
            $filename = end($file);
            return $this->response($filename, 'Image uploaded!');
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function activeInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'phone_number' => 'required'
        ]);

        try {
            $result = $transOrderService->activeInstant($postdata['phone_number']);
            return $this->response($result['data'], $result['message']);
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'invoice' => 'required',
            'phone_number' => 'required'
        ]);

        try {
            $result = $transOrderService->updateInstant($postdata['invoice'],[
                'is_proceed' => 1
            ]);
            (new DriverService())->setAvailable($postdata['phone_number'], 0);
            return $this->response(null, $result['message'].' | Accepted!');
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    public function rejectInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = [
            'invoice' => 'required',
            'phone_number' => 'required'
        ];

//        try {
//            $result = $transOrderService->rejec
//        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function doneInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'invoice' => 'required',
            'phone_number' => 'required',
            'drop_image' => 'required'
        ]);

        try {
            $result = $transOrderService->updateInstant($postdata['invoice'], [
                'drop_image' => $postdata['drop_image'],
                'drop_at' => printDateTime(),
                'is_done' => 1,
                'done_at' => printDateTime()
            ]);
            (new DriverService())->setAvailable($postdata['phone_number']);
            return $this->response(null, $result['message'].' | Delivered!');
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function pickupInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'invoice' => 'required',
            'pickup_image' => 'required'
        ]);

        try {
            $result = $transOrderService->updateInstant($postdata['invoice'], [
                'pickup_image' => $postdata['pickup_image'],
                'pickup_at' => printDateTime()
            ]);
            return $this->response(null, $result['message'].' | Picked!');
        } catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'invoice' => 'required',
            'phone_number' => 'required',
            'reason' => 'required'
        ]);

        try {
            $result = $transOrderService->updateInstant($postdata['invoice'], [
                'reason_canceled' => $postdata['reason'],
                'is_canceled' => 1,
                'canceled_at' => printDateTime()
            ]);
            (new DriverService())->setAvailable($postdata['phone_number']);
            return $this->response(null, $result['message'].' | Canceled!');
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param TransOrderService $transOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentInstant(Request $request, TransOrderService $transOrderService)
    {
        $postdata = $request->validate([
            'invoice' => 'required',
            'star' => 'required',
            'comment' => 'required'
        ]);

        try {
            $transOrderService->updateInstant($postdata['invoice'], [
                'star' => $postdata['star'],
                'comment' => $postdata['comment']
            ]);
            $result = $transOrderService->commentInstant($postdata['invoice'], [
                'star' => $postdata['star'],
                'comment' => $postdata['comment']
            ]);
            if($result['error'])
                return $this->error($result['message']);

            return $this->response(null, $result['message'].' | Commented!');
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
