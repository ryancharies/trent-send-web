<?php

namespace App\Http\Controllers\Api\Trans;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Trans\TransOtpRequest;
use App\Services\Trans\TransOtpService;
use Illuminate\Http\Request;

class TransOtpController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TransOtpRequest $request
     * @param TransOtpService $transOtpService
     * @return \Illuminate\Http\Response
     */
    public function store(TransOtpRequest $request, TransOtpService $transOtpService)
    {
        try {
            $result = $transOtpService->store($request->validated());
            return $this->response($result);
        }catch (\Exception $e){
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
