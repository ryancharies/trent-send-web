<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'is_online' => $this->is_online,
            'code' => $this->code,
            'phone_number' => $this->phone_number,
            'name' => $this->name,
            'token' => $this->token,
            'fcm_token' => $this->fcm_token
        ];
    }
}
