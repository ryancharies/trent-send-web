<?php

namespace App\Http\Resources\Master;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $path = imagePath('master/driver/');
        $default_image = asset('icon/avatar.png');
        $photo = is_null($this->photo) || !file_exists($path.$this->photo) ? $default_image : $path.$this->photo;

        return [
            'code' => $this->code,
            'phone_number' => '62'.$this->phone_number,
            'name' => $this->name,
            'star' => number_format($this->avgStar(),1),
            'photo' => $photo,
            'role' => 'Driver'
        ];
    }
}
