<?php

namespace App\Http\Resources\Master;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $path = imagePath('master/customer/');
        $default_image = asset('icon/avatar.png');
        $photo = is_null($this->photo) || !file_exists($path.$this->photo) ? $default_image : $path.$this->photo;

        return [
            'code' => $this->code,
            'phone_number' => '62'.$this->phone_number,
            'name' => $this->name,
            'photo' => $photo,
            'role' => 'Customer'
        ];
    }
}
