<?php

namespace App\Http\Resources\Trans;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailInstantDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status = ($this->is_done) ? 'Selesai' : 'Dalam Proses';
        $status = ($this->is_canceled) ? 'Dibatalkan' : $status;

        $is_done = ($this->is_done) ?: 2;
        $is_done = ($this->is_canceled) ? 0 : $is_done;

        $path = imagePath('master/customer/');
        $default_image = asset('icon/avatar.png');
        $default_item_image = asset('icon/nophoto.png');
        $photo = is_null($this->customer->photo) || !file_exists($path.$this->customer->photo) ? $default_image : $path.$this->customer->photo;
        $pickup_image = is_null($this->pickup_image) || !file_exists($path.$this->pickup_image) ? $default_item_image : $path.$this->pickup_image;
        $drop_image = is_null($this->drop_image) || !file_exists($path.$this->drop_image) ? $default_item_image : $path.$this->drop_image;

        return [
            'is_proceed' => $this->is_proceed,
            'is_done' => $is_done,
            'description' => $status,
            'invoice' => $this->invoice,
            'photo' => $photo,
            'name' => $this->customer->name,
            'payment_method' => $this->paymentMethod->description,
            'total_rate' => formatRupiah($this->total_rate),
            'distance' => $this->distance.' km',
            'latitude_sender' => $this->latitude_sender,
            'longitude_sender' => $this->longitude_sender,
            'address_sender' => $this->address_sender,
            'latitude_recipient' => $this->latitude_recipient,
            'longitude_recipient' => $this->longitude_recipient,
            'address_recipient' => $this->address_recipient,
            'description_item' => $this->description_item,
            'pickup_image' => $pickup_image,
            'drop_image' => $drop_image,
            'star' => $this->star,
            'comment' => $this->comment
        ];
    }
}
