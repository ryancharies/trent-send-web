<?php

namespace App\Http\Resources\Trans;

use Illuminate\Http\Resources\Json\JsonResource;

class ListInstantDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $status = ($this->is_done) ? 'Selesai' : 'Dalam Proses';
        $status = ($this->is_canceled) ? 'Dibatalkan' : $status;

        $is_done = ($this->is_done) ?: 2;
        $is_done = ($this->is_canceled) ? 0 : $is_done;

        $path = imagePath('master/customer/');
        $default_image = asset('icon/avatar.png');
        $photo = is_null($this->customer->photo) || !file_exists($path.$this->customer->photo) ? $default_image : $path.$this->customer->photo;

        return [
            'is_done' => $is_done,
            'description' => $status,
            'invoice' => $this->invoice,
            'photo' => $photo,
            'name' => $this->customer->name,
            'payment_method' => $this->paymentMethod->description,
            'total_rate' => formatRupiah($this->total_rate),
            'distance' => $this->distance.' km',
            'pickup' => $this->address_sender,
            'drop' => $this->address_recipient
        ];
    }
}
