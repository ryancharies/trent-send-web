<?php

namespace App\Http\Resources\Trans;

use App\Http\Resources\Master\DriverResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class DetailInstantCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $path = imagePath('trans/order/instant/');
        $default_image = asset('icon/box.png');
        $done_at = is_null($this->done_at) ? null : $this->done_at->format('d-m-Y H:i');
        $canceled_at = is_null($this->canceled_at) ? null : $this->canceled_at->format('d-m-Y H:i');
        $schedule_at = is_null($this->schedule_at) ? $this->order_date->format('d-m-Y H:i') : $this->schedule_at->format('d-m-Y H:i');
        $item_image = is_null($this->item_image) || !file_exists($path.$this->item_image) ? $default_image : $path.$this->item_image;

        return array(
            'is_proceed' => $this->is_proceed,
            'is_done' => $this->is_done,
            'information' => $this->when($this->is_done, 'Pengiriman Selesai', 'Pengiriman Dibatalkan'),
            'information_date' => $this->when($this->done, $done_at, $canceled_at),
            'reason_canceled' => $this->when(!$this->is_done, $this->reason_canceled, ''),
            'invoice' => $this->invoice,
            'schedule_at' => 'Pickup -> '.$schedule_at,
            'description_item' => Str::limit($this->description_item,20),
            'item_image' => $item_image,
            'recipient' => $this->recipient,
            'address_recipient' => $this->address_recipient,
            'phone_number_recipient' => $this->phone_number_recipient,
            'service' => $this->service->description,
            'payment_method' =>$this->paymentMethod->description,
            'height' => $this->height,
            'width' => $this->width,
            'length' => $this->length,
            'star' => $this->star,
            'comment' => $this->comment,
            'driver' => new DriverResource($this->driver)
        );
    }
}
