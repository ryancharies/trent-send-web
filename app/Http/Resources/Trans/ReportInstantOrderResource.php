<?php

namespace App\Http\Resources\Trans;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportInstantOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'invoice' => $this->invoice,
            'order_date' => $this->order_date->format('d-m-Y . H:i'),
            'customer' => $this->customer->name,
            'total_rate' => formatRupiah($this->total_rate),
            'driver' => is_null($this->driver->name) ? '-' : $this->driver->name,
            'is_done' => $this->is_done,
            'is_canceled' => $this->is_canceled
        ];
    }
}
