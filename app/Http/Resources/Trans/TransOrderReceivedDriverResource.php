<?php

namespace App\Http\Resources\Trans;

use Illuminate\Http\Resources\Json\JsonResource;

class TransOrderReceivedDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $item_image = is_null($this->item_image) || !file_exists($path.$this->item_image) ? $default_image : $path.$this->item_image;

        return [
            "invoice" => $this->invoice,
            "order_date" => formatDate($this->order_date),
            "sender" => $this->sender,
            "phone_number_sender" => $this->phone_number_sender,
            "latitude_sender" => $this->latitude_sender,
            "longitude_sender" => $this->longitude_sender,
            "address_sender" => $this->address_sender,
            "address_note_sender" => $this->address_note_sender,
            "recipient" => $this->recipient,
            "phone_number_recipient" => $this->phone_number_recipient,
            "latitude_recipient" => $this->latitude_recipient,
            "longitude_recipient" => $this->longitude_recipient,
            "address_recipient" => $this->address_recipient,
            "address_note_recipient" => $this->address_note_recipient,
            "description_item" => $this->description_item,
            "height" => $this->height,
            "width" => $this->width,
            "length" => $this->length,
            "item_image" => $item_image,
            "distance" => $this->distance.' km',
            "rate" => formatRupiah($this->rate),
            "total_rate" => formatRupiah($this->total_rate),
            "service" => $this->service->description,
            "payment_method" =>$this->paymentMethod->description
        ];
    }
}
