<?php

namespace App\Http\Resources\Trans;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class ListInstantCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $path = imagePath('trans/order/instant/');
        $default_image = asset('icon/box.png');
        $schedule_at = is_null($this->schedule_at) ? $this->order_date->format('d-m-Y H:i') : $this->schedule_at->format('d-m-Y H:i');
        $item_image = is_null($this->item_image) || !file_exists($path.$this->item_image) ? $default_image : $path.$this->item_image;

        return [
            'invoice' => $this->invoice,
//            'order_date' => $this->order_date->format('d-m-Y H:i'),
            'schedule_at' => 'Pickup -> '.$schedule_at,
            'is_done' => $this->is_done,
            'is_canceled' => $this->is_canceled,
            'description_item' => Str::limit($this->description_item,20),
            'item_image' => $item_image
        ];
    }
}
