<?php

namespace App\Http\Requests\Trans;

use Illuminate\Foundation\Http\FormRequest;

class TransOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_date' => 'required',
            'customer' => 'required',
            'sender' => 'required',
            'phone_number_sender' => 'required',
            'latitude_sender' => 'required',
            'longitude_sender' => 'required',
            'address_sender' => 'required',
            'address_note_sender' => 'required',
            'recipient' => 'required',
            'phone_number_recipient' => 'required',
            'latitude_recipient' => 'required',
            'longitude_recipient' => 'required',
            'address_recipient' => 'required',
            'address_note_recipient' => 'required',
            'description_item' => 'required',
            'height' => 'required',
            'width' => 'required',
            'length' => 'required',
            'item_image' => 'required',
            'distance' => 'required',
            'rate' => 'required',
            'total_rate' => 'required',
            'payment_method' => 'required',
            'service' => 'required'
        ];
    }
}
