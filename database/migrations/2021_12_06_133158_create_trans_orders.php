<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_orders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->dateTime('order_date')->useCurrent();
            $table->tinyInteger('is_scheduled')->default(0);
            $table->tinyInteger('is_done')->default(0);
            $table->tinyInteger('is_canceled')->default(0);
            $table->string('reason_canceled')->nullable();
            $table->string('delivery_status')->nullable()->default('Menghubungkan ke driver');
            $table->string('invoice');
            $table->foreignId('customer_id')->constrained('mst_customer');
            $table->string('sender');
            $table->string('phone_number_sender');
            $table->decimal('latitude_sender',10,8);
            $table->decimal('longitude_sender',11,8);
            $table->text('address_sender');
            $table->string('address_note_sender');
            $table->string('recipient');
            $table->string('phone_number_recipient');
            $table->decimal('latitude_recipient',10,8);
            $table->decimal('longitude_recipient',11,8);
            $table->text('address_recipient');
            $table->string('address_note_recipient');
            $table->foreignId('type_package_id')->nullable()->constrained('dt_type_package');
            $table->foreignId('size_id')->nullable()->constrained('dt_size');
            $table->string('description_item');
            $table->float('height')->default(0);
            $table->float('width')->default(0);
            $table->float('length')->default(0);
            $table->string('item_image')->nullable();
            $table->float('distance')->default(0);
            $table->string('uom_rate')->default('km');
            $table->float('rate')->default(0);
            $table->float('total_rate')->default(0);
            $table->string('pickup_image')->nullable();
            $table->string('drop_image')->nullable();
            $table->foreignId('service_id')->constrained('dt_services');
            $table->foreignId('payment_method_id')->constrained('dt_payment_method');
            $table->foreignId('driver_id')->nullable()->constrained('mst_driver');
            $table->tinyInteger('star')->nullable();
            $table->string('comment')->nullable();

            $table->index(['is_done','created_at']);
            $table->index(['service_id','payment_method_id']);
            $table->index('customer_id');
            $table->index('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_orders');
    }
}
