<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDtDriverComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dt_driver_comment', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('driver_id')->constrained('mst_driver');
            $table->string('invoice');
            $table->tinyInteger('star');
            $table->string('comment')->nullable();

            $table->index('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dt_driver_comment');
    }
}
