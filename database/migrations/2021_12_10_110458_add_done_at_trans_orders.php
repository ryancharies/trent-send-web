<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoneAtTransOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_orders', function (Blueprint $table) {
            $table->dateTime('pickup_at')->nullable()->after('pickup_image');
            $table->dateTime('drop_at')->nullable()->after('drop_image');
            $table->dateTime('canceled_at')->nullable()->after('is_canceled');
            $table->dateTime('schedule_at')->nullable()->after('is_scheduled');
            $table->dateTime('done_at')->nullable()->after('is_done');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_orders', function (Blueprint $table) {
            $table->dropColumn('pickup_at');
            $table->dropColumn('drop_at');
            $table->dropColumn('canceled_at');
            $table->dropColumn('schedule_at');
            $table->dropColumn('done_at');
        });
    }
}
