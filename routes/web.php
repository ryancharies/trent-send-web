<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    \App\Events\Master\LiveLocationDriverEvent::dispatch('haloo gan');
    return view('welcome');
});

Route::prefix('event')->group(function(){
    Route::get('/newTransOrder', function (){
        \App\Events\Trans\TransOrderCreatedEvent::dispatch('oke');
    });

    Route::get('/orderReceivedDriver', function (){
       \App\Events\Trans\TransOrderReceivedDriverEvent::dispatch('asdasda','DRV-BR06801');
    });
});

Route::get('/admin/login', [\App\Http\Controllers\AdminAuthController::class, 'login'])->name('getLogin');

//Route::get('/admin', [\App\Http\Controllers\AdminDashboardController::class, 'getIndex']);

Route::get('/admin/mobile_app_setting', [\App\Http\Controllers\AdminMobileAppSettingController::class, 'getIndex'])->name('AdminMobileAppSettingControllerGetIndex');
Route::post('/admin/mobile_app_setting/upload', [\App\Http\Controllers\AdminMobileAppSettingController::class, 'postUpload'])->name('AdminMobileAppSettingControllerPostUpload');

Route::get('/admin/mobile_app_setting/download/{apk_type}', [\App\Http\Controllers\AdminMobileAppSettingController::class, 'getDownload']);

Route::get('/admin/settings/rate', [\App\Http\Controllers\AdminRateSettingController::class, 'getIndex'])->name('AdminRateSettingControllerGetIndex');
Route::post('/admin/settings/rate', [\App\Http\Controllers\AdminRateSettingController::class, 'postSaveData'])->name('AdminRateSettingControllerPostSaveData');

Route::get('/admin/map/driver', [\App\Http\Controllers\AdminMapDriverController::class, 'getIndex'])->name('AdminMapDriverControllerGetIndex');

Route::get('/admin/trent_send', [\App\Http\Controllers\AdminTrentSendController::class, 'getIndex'])->name('AdminTrentSendControllerGetIndex');

Route::post('/admin_trent_send/cancel', [\App\Http\Controllers\AdminTrentSendController::class, 'postCancel'])->name('AdminTrentSendControllerPostCancel');
Route::post('/admin_trent_send/accept', [\App\Http\Controllers\AdminTrentSendController::class, 'postAccept'])->name('AdminTrentSendControllerPostAccept');
Route::post('/admin_trent_send/delete', [\App\Http\Controllers\AdminTrentSendController::class, 'postDelete'])->name('AdminTrentSendControllerPostDelete');

Route::get('/t', function () {
    event(new \App\Events\Trans\TransOrderCreatedEvent());
    dd('Event Run Successfully.');
});

Route::get('/driver_location', function () {
    event(new \App\Events\Master\DriverLocationEvent());
    return 'event fired!';
});
