<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('{user_role}/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
Route::post('customer/register', [\App\Http\Controllers\Api\AuthController::class, 'registerCustomer']);
Route::post('driver/register', [\App\Http\Controllers\Api\AuthController::class, 'registerDriver']);
Route::post('{user_role}/verifyOtp', [\App\Http\Controllers\Api\AuthController::class, 'verifyOtp']);
Route::post('{user_role}/resendOtp', [\App\Http\Controllers\Api\AuthController::class, 'resendOtp']);
Route::middleware('auth:sanctum')->get('{user_role}/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);

Route::middleware('auth:sanctum')->prefix('{user_role}/trans')->group(function () {
    Route::post('instant', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'storeInstant']);
    Route::post('instant/image', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'storeImage']);
    Route::get('instant/active', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'activeInstant']);
    Route::patch('instant/accept', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'acceptInstant']);
    Route::patch('instant/reject', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'rejectInstant']);
    Route::patch('instant/pickup', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'pickupInstant']);
    Route::patch('instant/done', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'doneInstant']);
    Route::patch('instant/cancel', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'cancelInstant']);
    Route::patch('instant/comment', [\App\Http\Controllers\Api\Trans\TransOrderController::class, 'commentInstant']);

    Route::get('instant/list',[\App\Http\Controllers\Api\Trans\TransOrderController::class, 'listInstant']);
    Route::get('instant/detail',[\App\Http\Controllers\Api\Trans\TransOrderController::class, 'detailInstant']);
});

Route::middleware('auth:sanctum')->prefix('driver')->group(function () {
    Route::get('profile', [\App\Http\Controllers\Api\Master\DriverController::class, 'profile']);
    Route::post('profile', [\App\Http\Controllers\Api\Master\DriverController::class, 'update']);
    Route::patch('status', [\App\Http\Controllers\Api\Master\DriverController::class, 'setStatus']);
});

Route::middleware('auth:sanctum')->prefix('customer')->group(function () {
    Route::get('profile', [\App\Http\Controllers\Api\Master\CustomerController::class, 'profile']);
    Route::post('profile', [\App\Http\Controllers\Api\Master\CustomerController::class, 'update']);
});
