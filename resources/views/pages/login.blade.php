<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trent Send: Login Page</title>
    <link rel="shortcut icon" href="{{ CRUDBooster::getSetting('favicon') ? asset(CRUDBooster::getSetting('favicon')) : asset('favicon.ico') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/iofrm-theme4.css') }}">
</head>
<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="#">
                <div class="logo">
                    <img class="logo-size" src="{{ asset(CRUDBooster::getSetting('logo')) }}" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="{{ asset('images/graphic1.svg') }}" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Trent Send Admin Panel.</h3>
                        <p>Kirim barang tanpa khawatir.</p>
                        <div class="page-links">
                            <a href="#" class="active">Masuk</a>
                        </div>
                        <form autocomplete="off" method="POST" action="{{ route('postLogin') }}">
                            @csrf
                            <input class="form-control" type="text" name="email" placeholder="E-mail Address" required>
                            <input class="form-control" type="password" name="password" placeholder="Password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Masuk</button>
{{--                                <button id="submit" type="submit" class="ibtn">Login</button> <a href="forget4.html">Forget password?</a>--}}
                            </div>
                        </form>
                        <hr>
                        <div class="other-links">
{{--                            <span>Or login with</span><a href="#">Facebook</a><a href="#">Google</a><a href="#">Linkedin</a>--}}
{{--                            <p class="text-center"><strong>App</strong></p>--}}
                            <div class="row">
                                <div class="col-lg-4">
                                    <a href="{{ url('admin/mobile_app_setting/download/pelanggan') }}" title="Download aplikasi untuk pelanggan">
                                        <img src="{{ asset('logo_customer_app.svg') }}" alt="logo_customer_app" height="100px" width="auto">
                                    </a>
                                </div>
                                <div class="col"></div>
                                <div class="col-lg-4">
                                    <a href="{{ url('admin/mobile_app_setting/download/driver') }}" title="Download aplikasi untuk driver">
                                        <img src="{{ asset('logo_driver_app.svg') }}" alt="logo_driver_app" height="100px" width="auto">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
