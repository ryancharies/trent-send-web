<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'><i class="fa fa-money"></i> Form {{ $page_title }}</div>
        <form method='post' action='{{ route('AdminRateSettingControllerPostSaveData') }} '>
            <div class='panel-body'>
                @csrf
                <div class='form-group'>
                    <label>Satuan Tarif<span class="text-danger">*</span></label>
                    <select class="form-control" name="uom_rate" id="uom_rate" required>
                        <option value="">**Silahkan pilih Satuan Tarif</option>
                        @foreach(uomRate('array') as $key => $item)
                            <option value="{{ $key }}" {{ $key == $row->where('name', 'uom_rate')->first()->content ? 'selected' : '' }}>{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class='form-group'>
                    <label>Kilometer Dasar<span class="text-danger">*</span></label>
                    <input type="text" name="min_distance" class="form-control" placeholder="cth: 15" value="{{ $row->where('name', 'min_distance')->first()->content }}" required>
                    <small><i>*jarak tempuh minimal yang dikenakan tarif dasar</i></small>
                </div>
                <div class='form-group'>
                    <label>Tarif Dasar<span class="text-danger">*</span></label>
                    <input type="text" name="basic_rate" class="form-control input-basic-rate" placeholder="cth: Rp 25.000" value="{{ $row->where('name', 'basic_rate')->first()->content }}" required>
                    <small><i>*tarif dasar yang dikenakan kilometer dasar pertama</i></small>
                </div>
                <div class='form-group'>
                    <label>Tarif Lanjutan<span class="text-danger">*</span></label>
                    <input type="text" name="rate" class="form-control input-rate" placeholder="cth: Rp 5.000" value="{{ $row->where('name', 'rate')->first()->content }}" required>
                    <small><i>*tarif selanjutnya yang dikenakan setelah kilometer dasar pertama</i></small>
                </div>
                <div class='form-group'>
                    <label><span class="text-danger">*</span> <i>Wajib diisi!</i></label>
                </div>
            </div>
            <div class='panel-footer'>
                <button type='submit' class='btn btn-success'>Simpan Pengaturan Tarif</button>
            </div>
        </form>
    </div>
@endsection
@push('bottom')
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0"></script>
    <script type="text/javascript">
        new AutoNumeric('.input-rate', {
            currencySymbol: 'Rp ',
            decimalCharacter: ',',
            digitGroupSeparator: '.'
        });
        new AutoNumeric('.input-basic-rate', {
            currencySymbol: 'Rp ',
            decimalCharacter: ',',
            digitGroupSeparator: '.'
        })
    </script>
@endpush
