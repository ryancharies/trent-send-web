<table>
    <thead>
    <tr>
        <th><strong>STATUS</strong></th>
        <th><strong>KODE</strong></th>
        <th><strong>NOMOR TELEPON</strong></th>
        <th><strong>NAMA</strong></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($rows as $item)
        <tr>
            <td>{{ transStatus($item['status']) }}</td>
            <td>{{ $item['code'] }}</td>
            <td>{{ $item['phone_number'] }}</td>
            <td>{{ $item['name'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
