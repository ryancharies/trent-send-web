<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
    <!-- Your custom  HTML goes here -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-body">
                    <div id='map' style='width: 100%; min-height: 600px;'></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('head')
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.css' rel='stylesheet' />
@endpush
@push('bottom')
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.js'></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        mapboxgl.accessToken = '{{ env('MAPBOX_TOKEN') }}';
        const map = new mapboxgl.Map({
            container: 'map', // container ID
            style: 'mapbox://styles/mapbox/streets-v11', // style URL
            center: [112.647441, -7.945870], // starting position [lng, lat]
            zoom: 12 // starting zoom
        });

        map.on('load', () => {
           map.loadImage('{{ asset('icon/moped.png') }}', (err, image) => {
               map.addImage('map-icon', image);
           });
        });

        map.addControl(new mapboxgl.FullscreenControl({container: document.querySelector('body')}));
        map.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true,
            showUserHeading: true
        }));

        var channel = Echo.channel('trentsend-app');
        channel.listen('.liveLocationDriverEvent', (data) => {
            console.log('data pusher: ', data);

            const geojson = {
                'type': 'FeatureCollection',
                'features': [
                    {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [data.lng, data.lat]
                        },
                        'properties': {
                            'title' : data.title
                        }
                    }
                ]
            };

            if(!map.getSource(data.id)){
                map.addSource(data.id, {
                    type: 'geojson',
                    data: geojson
                });
                map.addLayer({
                    'id': data.id,
                    'type': 'symbol',
                    'source': data.id,
                    'layout': {
                        'icon-image': 'map-icon',
                        'text-field': ['get', 'title'],
                        'text-font': [
                            'Open Sans Semibold',
                            'Arial Unicode MS Bold'
                        ],
                        'text-offset': [0, 1.25],
                        'text-anchor': 'top'
                    }
                });
            }else{
                map.getSource(data.id).setData(geojson);
            }
        });

    </script>
@endpush
