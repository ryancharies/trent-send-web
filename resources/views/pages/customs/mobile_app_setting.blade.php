<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'><i class="fa fa-android"></i> Upload Apk</div>
        <form method='post' action='{{ route("AdminMobileAppSettingControllerPostUpload") }}' enctype="multipart/form-data">
            @csrf
            <div class='panel-body'>
                <div class='form-group'>
                    <label for="file_apk_pelanggan">Aplikasi Trentsend Pelanggan</label>
                    <input type='file' name='file_apk_pelanggan' class='form-control'/>
                </div>
                <div class='form-group'>
                    <label for="file_apk_driver">Aplikasi Trentsend Driver</label>
                    <input type='file' name='file_apk_driver' class='form-control'/>
                </div>
            </div>
            <div class='panel-footer'>
                <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
            </div>
        </form>
    </div>
@endsection
