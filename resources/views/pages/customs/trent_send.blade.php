<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Permintaan Pengiriman</span>
                        <span class="info-box-number order-new-count">{{ $order['new'] }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pengiriman Terjadwal</span>
                        <span class="info-box-number order-schedule-count">{{ $order['scheduled'] }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Driver Online</span>
                        <span class="info-box-number driver-online-count">{{ $driver['online'] }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Driver Offline</span>
                        <span class="info-box-number driver-offline-count">{{ $driver['offline'] }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Permintaan Pengiriman Hari Ini</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool"><i class="fa fa-refresh"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="max-height: 400px">
                        <div class="table-responsive" style="overflow: auto; max-height: 350px">
                            <table class="table no-margin" id="tableNewTransOrder">
                                <thead>
                                <tr>
                                    <th>Invoice</th>
                                    <th>Tanggal</th>
                                    <th>Pengirim</th>
                                    <th>Status</th>
                                    <th>Isi Paket / Catatan</th>
                                    <th>Dimensi (cm)</th>
                                    <th>Foto</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order['list']['new'] as $item)
                                    <tr class="{{ $item->invoice }}">
                                        <td><a href="pages/examples/invoice.html">{{ $item->invoice }}</a></td>
                                        <td>{{ $item->order_date->format('d-m-Y H:i') }}</td>
                                        <td>{{ $item->sender }}</td>
                                        <td><span class="label label-success">{{ $item->delivery_status }}</span></td>
                                        <td>{{ $item->description_item }}</td>
                                        <td>{{ $item->length }} x {{ $item->width }} x {{ $item->height }}</td>
                                        <td><a href="{{ imagePathInstant($item->item_image) }}" class="popup-image"><img src="{{ imagePathInstant($item->item_image) }}" alt="item_image" class="image item_image"></a></td>
                                        <td>
                                            <button title="Terima" type="button" data-invoice="{{ $item->invoice }}" class="btn btn-xs btn-success btn-confirm" onclick="modalAcceptNew({{ $item }})"><i class="fa fa-check"></i></button>
                                            <button title="Tolak" type="button" data-invoice="{{ $item->invoice }}" class="btn btn-xs btn-danger btn-action-cancel" onclick="modalCancelNew('{{ $item->invoice }}')"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Permintaan Pengiriman Terjadwal</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool"><i class="fa fa-refresh"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="max-height: 400px">
                        <div class="table-responsive" style="overflow: auto; max-height: 350px">
                            <table class="table no-margin" id="tableScheduleTransOrder">
                                <thead>
                                    <tr>
                                        <th>Invoice</th>
                                        <th>Tanggal</th>
                                        <th>Pelanggan</th>
                                        <th>Status</th>
                                        <th>Isi Paket / Catatan</th>
                                        <th>Dimensi (cm)</th>
                                        <th>Foto</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order['list']['schedule'] as $item)
                                        <tr class="{{ $item->invoice }}">
                                            <td><a href="pages/examples/invoice.html">{{ $item->invoice }}</a></td>
                                            <td>{{ $item->schedule_at->format('d-m-Y H:i') }}</td>
                                            <td>{{ $item->sender }}</td>
                                            <td><span class="label label-{{ $item->schedule_at->lt(\Carbon\Carbon::now()) ? 'danger' : 'success' }}">{{ $item->schedule_at->lt(\Carbon\Carbon::now()) ? 'Kadaluarsa' : $item->delivery_status  }}</span></td>
                                            <td>{{ $item->description_item }}</td>
                                            <td>{{ $item->length }} x {{ $item->width }} x {{ $item->height }}</td>
                                            <td><a href="{{ imagePathInstant($item->item_image) }}" class="popup-image"><img src="{{ imagePathInstant($item->item_image) }}" alt="item_image" class="image item_image"></a></td>
                                            <td>
                                                @if($item->schedule_at->lt(\Carbon\Carbon::now()))
                                                    <button title="Hapus" type="button" class="btn btn-xs btn-danger btn-delete" onclick="modalDeleteSchedule('{{ $item->invoice }}')"><i class="fa fa-trash"></i></button>
                                                @else
                                                    <button title="Terima" type="button" class="btn btn-xs btn-success btn-confirm" onclick="modalAcceptNew({{ $item }})"><i class="fa fa-check"></i></button>
                                                    <button title="Tolak" type="button" class="btn btn-xs btn-danger btn-action-cancel" onclick="modalCancelNew('{{ $item->invoice }}')"><i class="fa fa-times"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content-wrapper -->

    <!-- Modal Cancel -->
    <div class="modal fade" id="modalCancel" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content panel-danger">
                <div class="modal-header panel-heading">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tolak Pengiriman</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <input type="text" name="invoice" id="invoice" readonly class="form-control">
                        </div>
                        <div class="form-group">
                            <textarea name="reason_canceled" id="reason_canceled" cols="30" rows="10" class="form-control" placeholder="Berikan alasan penolakan..."></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-cancel">Ya, proses penolakan</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cancel -->
    <div class="modal fade" id="modalAccept" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content panel-primary">
                <div class="modal-header panel-heading">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Terima Pesanan dan Pilih Driver</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="text-center">Informasi Pengiriman</h4>
                            <div class="form-group">
                                <label for="invoice">Invoice</label>
                                <input type="text" id="invoice-accept" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label for="recipient">Penerima</label>
                                <input type="text" id="recipient" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label for="address_sender">Alamat Pengambilan</label>
                                <textarea readonly name="address_sender" id="address_sender" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="address_recipient">Alamat Pengiriman</label>
                                <textarea readonly name="address_recipient" id="address_recipient" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="total_rate">Ongkos Kirim</label>
                                <input type="text" id="total_rate" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="text-center">Driver yang tersedia</h4>
                            <div class="form-group" style="height: auto">
                                <div class="list-group" id="list_available_driver" style="overflow: auto; height: 350px">
                                    @if(is_null($driver['list']))
                                        <p>Belum ada driver tidak dalam posisi siap</p>
                                    @endif
                                    @foreach($driver['list'] as $item)
                                        <div class="{{ $item->code }}">
                                            <input type="radio" name="selectedDriver" value="{{ $item->id }}" id="{{ $item->code }}"/>
                                            <label class="list-group-item" for="{{ $item->code }}">{{ $item->name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-accept">Atur pengiriman</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('head')
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" integrity="sha512-+EoPw+Fiwh6eSeRK7zwIKG2MA8i3rV/DGa3tdttQGgWyatG/SkncT53KHQaS5Jh9MNOT3dmFL0FjTY08And/Cw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@push('bottom')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/autonumeric@4.5.4"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js" integrity="sha512-IsNh5E3eYy3tr/JiX2Yx4vsCujtkhwl7SLqgnwLNgf04Hrt9BT9SXlLlZlWx+OK4ndzAoALhsMNcCmkggjZB1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        let orderNewCount = parseInt($('.order-new-count').html());
        let orderScheduleCount = parseInt($('.order-schedule-count').html());
        let driverOnlineCount = parseInt($('.driver-online-count').html());
        let driverOfflineCount = parseInt($('.driver-offline-count').html());

        var newOrderCreated = Echo.channel('trentsend-app');
        newOrderCreated.listen('.transOrderCreatedEvent', (data) => {
            console.log('event transOrderCreatedEvent:', data);
            const audio = new Audio("{{ url('sound/new_order.mp3') }}");
            audio.play();

            const postdata = data.postdata;
            const item_image = "{{ imagePathInstant('') }}/" + postdata.item_image;
            let newRow = `<tr class="${postdata.invoice}">`+
                            `<td><a href="pages/examples/invoice.html">${postdata.invoice}</a></td>`+
                            `<td>${moment(postdata.order_date).format('DD-MM-YYYY hh:mm')}</td>`+
                            `<td>${postdata.sender}</td>`+
                            `<td><span class="label label-success">${postdata.delivery_status}</span></td>`+
                            `<td>${postdata.description_item }</td>`+
                            `<td>${postdata.length} x ${postdata.width} x ${postdata.height}</td>`+
                            `<td><a href="${item_image}" class="popup-image"><div class="item-image-${postdata.invoice}"></div></a></td>`+
                            `<td>`+
                                `<button title="Terima" type="button" data-invoice="${postdata.invoice}" class="btn btn-xs btn-success btn-confirm" onclick='modalAcceptNew({ invoice: "${postdata.invoice}", recipient: "${postdata.recipient}", address_sender: "${postdata.address_sender}", address_recipient: "${postdata.address_recipient}", total_rate: "${postdata.total_rate}" })'><i class="fa fa-check"></i></button> `+
                                `<button title="Tolak" type="button" data-invoice="${postdata.invoice}" class="btn btn-xs btn-danger btn-action-cancel" onclick="modalCancelNew('${postdata.invoice}')"><i class="fa fa-times"></i></button>`+
                            `</td>`+
                        `</tr>`;
            if(postdata.is_scheduled){
                $('#tableScheduleTransOrder > tbody').append(newRow);
                orderScheduleCount += 1;
                $('.order-schedule-count').html(orderScheduleCount);
            }else{
                $('#tableNewTransOrder > tbody').append(newRow);
                orderNewCount += 1;
                $('.order-new-count').html(orderNewCount);
            }
            $(`<img src='${item_image}' alt="item_image" class="image item_image">`).appendTo('.item-image-'+postdata.invoice);
            popupImage();
        });

        var driverOnline = Echo.channel('trentsend-app');
        driverOnline.listen('.driverOnlineEvent', (data) => {
            console.log('event driverOnlineEvent:', data);

            const postdata = data.postdata;
            driverOnlineCount = postdata.is_online ? Math.max(0, driverOnlineCount+1) : Math.max(0, driverOnlineCount-1);
            driverOfflineCount = postdata.is_online ? Math.max(0, driverOfflineCount-1) : Math.max(0, driverOfflineCount+1);
            $('.driver-online-count').html(driverOnlineCount);
            $('.driver-offline-count').html(driverOfflineCount);

            if(!postdata.is_online){
                $(`.${postdata.code}`).remove();
            }
            if(postdata.is_online || postdata.is_available){
                let updateRow = `<div class="${postdata.code}">`+
                    `<input type="radio" name="selectedDriver" value="${postdata.id}" id="${postdata.code}"/>`+
                    `<label class="list-group-item" for="${postdata.code}">${postdata.name}</label>`+
                    `</div>`;
                $('#list_available_driver').append(updateRow);
            }
        });

        function modalAcceptNew(postdata){
            $('#invoice-accept').val(postdata.invoice);
            $('#recipient').val(postdata.recipient);
            $('#address_sender').html(postdata.address_sender);
            $('#address_recipient').html(postdata.address_recipient);
            $('#total_rate').val(postdata.total_rate);
            new AutoNumeric('#total_rate', {
                currencySymbol: 'Rp ',
                decimalCharacter: ',',
                digitGroupSeparator: '.',
                decimalPlaces: 0
            });

            $('#modalAccept').modal('toggle');
        }

        function modalCancelNew(invoice = null){
            $('#invoice').val(invoice);
            $('#modalCancel').modal('toggle');
        }

        function modalDeleteSchedule(invoice = null){
            Swal.fire({
                title: 'Hapus permintaan pengiriman?',
                text: "Data akan dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#6D8299',
                cancelButtonText: 'Tidak jadi',
                confirmButtonText: 'Ya, hapus data!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('AdminTrentSendControllerPostDelete') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            invoice: invoice
                        },
                        success: (res) => {
                            console.log('cancel response: ', res);
                            Swal.fire(
                                'Good job!',
                                res.message,
                                'success'
                            );
                            $(`.${invoice}`).remove();
                            orderScheduleCount -= 1;
                            $('.order-schedule-count').html(orderScheduleCount);
                        },
                        error: (err) => {
                            console.log('error response: ', err);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops! Something went wrong!',
                                text: err.message,
                            });
                        }
                    });
                }
            })
        }

        function popupImage(){
            $('.popup-image').magnificPopup({type:'image'});
        }

        $('.btn-cancel').on('click', () => {
            let invoice = $('#invoice').val();
            let button = $(this);
            let reason_canceled = $('#reason_canceled').val();
            if(reason_canceled == ''){
                Swal.fire(
                    'Pesan!',
                    'Wajib menyertakan alasan pembatalan!',
                    'warning'
                );
                return;
            }
            $.ajax({
                method: "POST",
                url: "{{ route('AdminTrentSendControllerPostCancel') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    invoice: invoice,
                    reason_canceled: reason_canceled
                },
                beforeSend: () => {
                    button.html('<i class="fa fa-spinner fa-spin"></i> Memproses...');
                    button.prop('disabled', true);
                },
                success: (res) => {
                    console.log('cancel response: ', res);
                    Swal.fire(
                        'Good job!',
                        res.message,
                        'success'
                    );
                    $('#modalCancel').modal('hide');
                    button.html('Ya, proses penolakan');
                    button.prop('disabled', false);
                    $(`.${invoice}`).remove();
                    $('#reason_canceled').val('');
                },
                error: (err) => {
                    console.log('error response: ', err.message);
                    button.html('Ya, proses penolakan');
                    button.prop('disabled', false);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops! Something went wrong!',
                        text: err.message,
                    });
                }
            });
        });

        $('.btn-accept').on('click', () => {
            let invoice = $('#invoice-accept').val();
            let button = $(this);
            let selectedDriver = $('input[name="selectedDriver"]:checked').val();
            if(!selectedDriver){
                Swal.fire(
                    'Pesan!',
                    'Silahkan pilih driver terlebih dahulu!',
                    'warning'
                );
                return;
            }
            $.ajax({
                method: "POST",
                url: "{{ route('AdminTrentSendControllerPostAccept') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    invoice: invoice,
                    driver: selectedDriver
                },
                beforeSend: () => {
                    button.html('<i class="fa fa-spinner fa-spin"></i> Memproses...');
                    button.prop('disabled', true);
                },
                success: (res) => {
                    console.log('cancel response: ', res);
                    Swal.fire(
                        'Good job!',
                        res.message,
                        'success'
                    );
                    $('input[name="selectedDriver"]:checked').closest('div').remove();
                    $('#modalAccept').modal('hide');
                    button.html('Atur pengiriman');
                    button.prop('disabled', false);
                    $(`.${invoice}`).remove();
                },
                error: (err) => {
                    console.log('error response: ', err);
                    button.html('Atur pengiriman');
                    button.prop('disabled', false);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops! Something went wrong!',
                        text: err.message,
                    });
                }
            })
        });

        $(document).ready(function (){
            popupImage();
        })

    </script>
@endpush
