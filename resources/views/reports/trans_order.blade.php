<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
    <!-- Your custom  HTML goes here -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Histori Pengiriman</h3>

                    <div class="box-tools pull-right">
                        <button type="button" title="Print" class="btn btn-box-tool btn-print"><i class="fa fa-print"></i>
                        </button>
                        <form style="display: none;" action="{{ CRUDBooster::mainpath('print') }}" id="form-print" method="POST" target="_blank" >
                            @csrf
                            <input type="hidden" name="_start" id="_start">
                            <input type="hidden" name="_end" id="_end">
                            <input type="hidden" name="_status" id="_status">
                        </form>
                        <button type="button" title="Segarkan" class="btn btn-box-tool btn-refresh"><i class="fa fa-refresh"></i>
                        </button>
                        <button type="button" title="Sembunyikan" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-sm-6 col-md-4 col-lg-2">
                            <label for="date">Tanggal</label>
                            <input type="text" class="form-control dtrangepicker" id="date">
                        </div>
                        <div class="form-group col-sm-6 col-md-4 col-lg-2">
                            <label for="status">Status</label>
                            <select id="status" class="form-control">
                                <option value="all" selected>**Silahkan pilih Status</option>
                                <option value="done">Selesai</option>
                                <option value="cancel">Dibatalkan</option>
                                <option value="expired">Kadaluarsa</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <table id="table-report" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Invoice</th>
                                <th>Tanggal & Jam</th>
                                <th>Pelanggan</th>
                                <th>Ongkos Kirim</th>
                                <th>Status</th>
                                <th>Driver</th>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@push('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.css">
@endpush

@push('bottom')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-easy-loading@1.3.0/dist/jquery.loading.min.js"></script>
    <script>
        let start = moment().startOf('month');
        let end = moment().endOf('month');
        $('#_start').val(start.format('DD-MM-YYYY')); $('#_end').val(end.format('DD-MM-YYYY'));

        $('.dtrangepicker').daterangepicker({
            autoApply: true,
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 hari terakhir': [moment().subtract(6, 'days'), moment()],
                '30 hari terakhir': [moment().subtract(29, 'days'), moment()],
                'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                    'month').endOf('month')]
            },
            locale: {
                customRangeLabel: 'Kustom',
                applyLabel: 'Atur',
                cancelLabel: 'Batal',
                format: 'DD-MM-YYYY'
            }
        }, function(s, e){
            start = moment(s); end = moment(e);
            $('#_start').val(s); $('#_end').val(e);
        });

        $('.dtrangepicker').on('apply.daterangepicker', function () {
            table.ajax.reload();
        });

        $('#status').change(() => {
            table.ajax.reload();
        });

        $('.btn-refresh').on('click',() => {
            table.ajax.reload();
        });

        $('.btn-print').on('click',() => {
            $('#_status').val($('#status').val());
            $('#form-print').submit();
        });

        let table = $('#table-report').on('preXhr.dt', () => {
            $('.box').loading();
        }).on('xhr.dt', () => {
            $('.box').loading('stop');
        }).DataTable({
            ajax: {
                url: '{{ CRUDBooster::mainpath("datatable") }}',
                data: function(param){
                    param.start = start.format('DD-MM-YYYY');
                    param.end = end.format('DD-MM-YYYY');
                    param.status = $('#status').val();
                }
            },
            columns: [
                {data: 'invoice'},
                {data: 'order_date'},
                {data: 'customer'},
                {data: 'total_rate'},
                {data: 'delivery_status'},
                {data: 'driver'}
            ],
            language: {
                url: "{{ asset('plugin/datatable/language-id.json') }}"
            }
        });
    </script>
@endpush
