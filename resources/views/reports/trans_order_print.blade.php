@extends('template.reports_template')
@section('content')
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <img src="{{ asset('logo-trent-send.png') }}" alt="trent-send" height="80px">
                    <div class="pull-right">
                        Laporan Pengiriman <br>
                        <small>Periode : {{ $date }}</small>
                    </div>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
{{--        <div class="row invoice-info">--}}
{{--            <div class="col-sm-4 invoice-col">--}}
{{--                <address>--}}
{{--                    <strong>Admin, Inc.</strong><br>--}}
{{--                    795 Folsom Ave, Suite 600<br>--}}
{{--                    San Francisco, CA 94107<br>--}}
{{--                    Phone: (804) 123-5432<br>--}}
{{--                    Email: info@almasaeedstudio.com--}}
{{--                </address>--}}
{{--            </div>--}}
{{--            <!-- /.col -->--}}
{{--            <div class="col-sm-4 invoice-col">--}}
{{--                To--}}
{{--                <address>--}}
{{--                    <strong>John Doe</strong><br>--}}
{{--                    795 Folsom Ave, Suite 600<br>--}}
{{--                    San Francisco, CA 94107<br>--}}
{{--                    Phone: (555) 539-1037<br>--}}
{{--                    Email: john.doe@example.com--}}
{{--                </address>--}}
{{--            </div>--}}
{{--            <!-- /.col -->--}}
{{--            <div class="col-sm-4 invoice-col">--}}
{{--                <b>Invoice #007612</b><br>--}}
{{--                <br>--}}
{{--                <b>Order ID:</b> 4F3S8J<br>--}}
{{--                <b>Payment Due:</b> 2/22/2014<br>--}}
{{--                <b>Account:</b> 968-34567--}}
{{--            </div>--}}
{{--            <!-- /.col -->--}}
{{--        </div>--}}
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Invoice</th>
                            <th>Tanggal & Jam</th>
                            <th>Pelanggan</th>
                            <th>Ongkos Kirim</th>
                            <th>Status</th>
                            <th>Driver</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($rows as $row)
                        <tr>
                            <td>{{ $row['invoice'] }}</td>
                            <td>{{ $row['order_date'] }}</td>
                            <td>{{ $row['customer']['name'] }}</td>
                            <td>{{ formatRupiah($row['total_rate']) }}</td>
                            <td>{{ transDeliveryStatus($row['is_done'], $row['is_canceled']) }}</td>
                            <td>{{ $row['driver'] ?? '-' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
{{--                <p class="lead">Payment Methods:</p>--}}
{{--                <img src="../../dist/img/credit/visa.png" alt="Visa">--}}
{{--                <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">--}}
{{--                <img src="../../dist/img/credit/american-express.png" alt="American Express">--}}
{{--                <img src="../../dist/img/credit/paypal2.png" alt="Paypal">--}}

{{--                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">--}}
{{--                    Data transaksi berikut merupakan data yang bersumber dari histori transaksi.--}}
{{--                </p>--}}
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
{{--                <p class="lead">Amount Due 2/22/2014</p>--}}

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Status:</th>
                            <td>{{ $status }}</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>{{ formatRupiah($sumTotalRate) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
